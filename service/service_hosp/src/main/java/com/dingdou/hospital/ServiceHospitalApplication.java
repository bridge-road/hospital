package com.dingdou.hospital;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author: ding dou
 * @className: ServiceHospitalApplication
 * @packageName: com.dingdou.hospital
 * @description: 医院启动类
 **/
@SpringBootApplication
@MapperScan("com.dingdou.hospital.mapper")
@ComponentScan(basePackages = "com.dingdou")
public class ServiceHospitalApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceHospitalApplication.class, args);
    }
}

