package com.dingdou.hospital.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dingdou.hospital.model.hosp.HospitalSet;

/**
 * @ProjectName: hospital
 * @Package: com.dingdou.hospital.service
 * @ClassName: HospitalSetService
 * @Author: DingDou
 * @Description:
 */
public interface HospitalSetService extends IService<HospitalSet> {
}
