package com.dingdou.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dingdou.hospital.model.hosp.HospitalSet;

/**
 * @ProjectName: hospital
 * @Package: com.dingdou.hospital.mapper
 * @ClassName: HospitalSetMapper
 * @Author: DingDou
 * @Description:
 */
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {
}
