package com.dingdou.hospital.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dingdou.hospital.model.hosp.HospitalSet;
import com.dingdou.hospital.mapper.HospitalSetMapper;
import com.dingdou.hospital.service.HospitalSetService;
import org.springframework.stereotype.Service;

/**
 * @author: ding dou
 * @className: HospitalSetServiceImpl
 * @packageName: com.dingdou.hospital.service.impl
 * @description:
 **/
@Service
public class HospitalSetServiceImpl extends ServiceImpl<HospitalSetMapper, HospitalSet>implements HospitalSetService {

}

